# SQL

De kunskaper du ska inhämta till detta kursmoment hittar du på adressen: https://www.w3schools.com/sql/default.asp

## Övningar

Övningsuppgifter hittar du på adressen: https://www.w3schools.com/sql/exercise.asp

## Självtest

Självtestet hittar du på adressen: https://www.w3schools.com/sql/sql_quiz.asp