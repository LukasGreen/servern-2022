# Git - Uppgift 4

I denna uppgift ska du lägga till mig som "Reporter" i ditt repository så att jag kan se din kod

## Instruktioner

- Gå in på "members"-sektionen
- Sök upp mig, mitt användarnamn är `hambern`
- Ge mig rollen `Reporter` och se till att jag har tillgång till ditt repository till åtminstone juli nästa sommar

