# GIT - Uppgift 5

I denna uppgift ska du visa att du lärt dig grundläggande git-funktioner. Läs följande sida på W3Schools:

- <https://www.w3schools.com/git/git_staging_environment.asp?remote=gitlab>
- <https://www.w3schools.com/git/git_commit.asp?remote=gitlab>
- <https://www.w3schools.com/git/git_revert.asp?remote=gitlab>
- <https://www.w3schools.com/git/git_reset.asp?remote=gitlab>
- <https://www.w3schools.com/git/git_amend.asp?remote=gitlab>

## Instruktioner

Organisera din webbserverprogrammeringsmapp så att den innehåller följande mappar såhär:

~~~
├── git
├── linux
├── projekt
└── sql
~~~

I mappen `git` ska du skapa en fil som heter `uppgift_5.txt` däri ska du svara på följande frågor och göra en ny commit efter varje fråga. Svara på fråga 1, gör en ny commit, svara på fråga 2, gör en ny commit och fortsätt så. Glöm inte att testa alla kommandon.

1. Vad innebär det att "committa" och vad skriver du för att göra det?
2. Om du har gjort ändringar i en fil men vill ta bort allt som är "modified" hur skriver man då? Testa att det fungerar.
3. Om du har lagt till en fil men vill ta bort den från "staged" area. Hur skriver du för att ta bort filen? Testa så att det fungerar.
4. Om du har lagt till en fil och "commitat" den! Hur gör man om man vill radera denna commit? Testa så att det fungerar.
5. Läs på, testa och förklara vad `git commit --amend` gör.
6. Läs på, testa och förklara vad `git diff --cached` gör.

Gå in på filen på GitLab, kopiera URL-adressen och lämna in den på uppgiften på Vklass. Jag ska kunna klicka på länken och läsa svaren på ovanstående frågor.
