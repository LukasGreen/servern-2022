# Linux - Uppgift 2

## Instruktioner

I denna uppgift ska du få bekanta dig med textredigeraren Vim. Det är en textredigerare du kan använda direkt i terminalen.

1. Öppna en SSH-session och skriv in kommandot `vimtutor sv`.
2. Spara ditt dokument i ditt git-repository under mappen `linux` med namnet `tutor` med kommandot `:w ~/public_html/webbserverprogrammering_1/linux/tutor`
3. Genomför alla övningar fram till och med lektion 4 och rätta till alla exempel. Lektion 5-8 kan du göra i mån av tid och lust.
4. Spara det färdiga dokumentet med kommandot `:w` i Vim
5. Gitta in din kod med följande rader:

~~~~
git status
git add --all
git commit -m "linux_uppgift_2"
git pull
git push
~~~~
