# Linux - Uppgift 1

## Kommandon

Vad gör följande kommandon? Skapa ett dokument där du skriver på svenska en mening vardera om vad följande kommandon gör. Ett tips är att du kan använda linux inbyggda kommandomanual `man`. Den kommer du åt genom att skriva `man` följt av det kommando du vill veta mer om. Du kan också använda denna sida: <https://sunlightmedia.org/sv/grundl%C3%A4ggande-kommandon-i-linux/>

1. `man`
2. `pwd`
3. `cd`
4. `cd ..`
5. `whoami`
6. `mkdir`
7. `ls`
8. `ll`
9. `cat`
10. `more`
11. `less`
12. `echo`
13. `touch`
14. `mv`
15. `cp`
16. `rm`
17. `rmdir`
18. `wc`
19. `tree`
20. `vim`
21. `vimtutor`

## Bra att veta

Det är inte bara alla kommandon som är bra att veta. Det är också bra att veta hur själva terminalen fungerar:

22.  Vad gör piltangenterna upp och ner?
23.  Vad gör `tab`-tangenten i terminalen?
24.  Vad gör kortkommandot `Ctrl + C`?
25.  Vad gör kortkommandot `Ctrl + Z`?