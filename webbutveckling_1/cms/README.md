# CMS

I detta kursmoment ska vi installera och redigera en hemsida med verktyget Wordpress

## Instruktioner

- Besök sidan: https://sv.wordpress.org/
- Läs på hur verktyget fungerar och vad det gör
- Använd installationsinstruktionerna du hittar på: https://wordpress.org/support/article/how-to-install-wordpress/