# HTML - Uppgift 2

I denna uppgift får du:

- En [png-bild](facit.png) på den färdiga sidan
- En [css-fil](style.css) med CSS-kod. Denna får du _inte_ röra. Däremot får du gärna titta på den.
- En [html-fil](index.html) där väsentliga taggarna tagits bort
- En [php-fil](action.php) där formulärets data granskas. Denna får du _inte_ röra. Däremot får du gärna titta på den.

## Instruktioner

I denna uppgift ska du skapa ett formulär och använda det för att skicka information

Läs på om hur du gör formulär på:

- <https://www.w3schools.com/html/html_forms.asp>
- <https://www.w3schools.com/html/html_forms_attributes.asp>

Skapa ett formulär som ser ut som facit. Detta innebär följande:

1. Koppla html-filen till `style.css` i head-sektionen
2. Ange med hjälp av taggar rubriken till rubriknivå 1
3. Koppla formuläret `<form>` till php-dokumentet `action.php` och skicka informationen via metoden `post`
4. När du skapar ditt formulär bör du använda följande taggar:
   1. `<p>` - stycke
   2. `<input>` - inputfält
   3. `<label>` - fältrubrik
   4. `<br>` - radbrytning
5. Koppla fältrubriken till inputfältet så att fältet markeras när du klickar på fältrubriken
6. Namnge fälten korrekt så att namnen motsvarar de som förväntas i dokumentet `action.php`
7. Skickaknappen ska bestå av ett inputfält `<input>` i ett eget stycke `<p>`
8. När du är klar zippar du hela din mapp och lämnar in

### Bedömning

Denna uppgifts bedöms med antingen "Klarat", "Genomfört" eller "Inte genomfört"

## Extrauppgifter

1. När du är klar kan du lägga till några extra-fält i ditt formulär. En lista på sådana hittar du på: <https://www.w3schools.com/html/html_form_input_types.asp>
2. Studera `action.php` och försök lista ut hur filen fungerar och gör att den skriver ut informationen från dina nya fält också

Lämna gärna in även din specialdesignade sida
