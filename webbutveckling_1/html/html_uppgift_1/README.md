# HTML - Uppgift 1

I denna uppgift får du:

- En [png-bild](facit.png) på den färdiga sidan
- En [css-fil](style.css) med CSS-kod. Denna får du _inte_ röra. Däremot får du gärna titta på den.
- En [html-fil](index.html) där taggarna tagits bort

## Instruktioner

Din uppgift är att skriva tillbaka rätt taggar på rätt ställe så att din hemsida ser ut som bilden (vissa avvikelser kommer förekomma).

När du är klar packar du hela din mapp till en zip-fil och lämnar in den

### Element

De element som är bra att känna till är:

- Länk: `a`
- Bild: `img`
- Stycke: `p`
- Block: `div`
- Kursiv text: `i`
- Rubriker: `h1`, `h2`, `h3`
- Tabeller: `table`, `th`, `tr`, `td`
- Listor: `ol`, `ul`, `li`
- Avdelare: `hr`

### Tips

- Läs på om hur du skriver HTML på: https://www.w3schools.com/html/html_basic.asp
- Det enda attribut du behöver använda är `href` i länktaggen
- Tänk på att [indentera](https://www.freecodecamp.org/news/how-to-indent-in-html-and-why-it-is-important/) din kod så blir det lättare att hitta eventuella fel

### Bedömning

Denna uppgifts bedöms med antingen "Klarat", "Genomfört" eller "Inte genomfört"

## Extrauppgifter

När du är klar och har lämnat in kan du:

1. Leka med designen, ändra gärna i css-filen och se vad som händer
2. Lägg in passande bilder till artikeln. Använd gärna sidan https://unsplash.com där du kan hitta bilder som är fria att använda

### Tips

Du kan göra bilderna responsiva genom att lägga till följande kod till css-filen:

~~~css
img {
    width: 100%;
}
~~~

Lämna gärna in även din specialdesignade sida