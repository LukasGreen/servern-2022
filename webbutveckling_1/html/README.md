# HTML

De kunskaper du ska inhämta till detta kursmoment hittar du på adressen: https://www.w3schools.com/html/default.asp

## Övningar

Övningsuppgifter hittar du på adressen: https://www.w3schools.com/html/html_exercises.asp

## Självtest

Självtestet hittar du på adressen: https://www.w3schools.com/html/html_quiz.asp

## Uppgifter

1. [html_uppgift_1](html_uppgift_1)
2. [html_uppgift_2](html_uppgift_2)