# CSS - Uppgift 1

Din uppgift är att skapa ett externt CSS-dokument som formaterar ett på förhand givet HTML5-dokument. Använd gärna följande delar i W3Schools:

- <https://www.w3schools.com/css/css_syntax.asp>
- <https://www.w3schools.com/css/css_selectors.asp>
- <https://www.w3schools.com/css/css_howto.asp>
- <https://www.w3schools.com/css/css_boxmodel.asp>
- <https://www.w3schools.com/cssref/default.asp>

## Instruktioner

- Filens namn ska vara `style.css`
- CSS-filen `style.css` ska vara ett externt CSS
- All formatering ska ske i filen `style.css`
- HTML-koden i `ìndex.html` skall användas och skall inte ändras, alla formatering ska ske via `style.css`
- Facit kan du se via: [facit.jpg](facit.jpg) (visar hur webbsidan ser ut i webbläsaren Firefox).
- När du är klar packar du hela din mapp till en zip-fil och lämnar in den

## Följande gäller för sidan och ingående delars layout

### Allmänt

- `body` - Bakgrundsfärgen skall vara #E5E5E5. Texten skall ha typsnittet "Trebuchet MS" och därefter Helvetica, Arial och sans-serif. Texten skall ha storleken 0.8 em och ha färgen #6C6C6C.
- `a` - Skall inte vara understrukna i utgångsläget, men understrukna då muspekaren förs över.
- `ul` - Skall ha en svart prickad ram på 1px, en marginal på 1em, en utfyllnad på 1em, listans punkter ska ligga innanför ramen, texten skall vara kursiv och textstorleken 0.8em

### Sidans indelning

- `article` - skall ha vit som bakgrundsfärg. Den skall ha bredden 800 pixlar och ha en heldragen ram som är 1 pixel bred och har färgen #666666. Elementet skall vara centrerat i sidan.
- `header` - skall ha en utfyllnad på 10 pixlar, ha en marginal på 3 pixlar åt alla håll utom nedåt där marginalen skall vara 20 pixlar. Den skall vidare ha en prickad ram som är 2 pixlar tjock och har färgen #999999. Rubriker av typen h1 inom "header" skall ha färgen #FFAA01 och vara av storleken 2em. All text i sidhuvudet skall vara högerjusterad.
- `section` - skall ha en utfyllnad som är 10 pixlar uppåt och nedåt samt 20 pixlar åt sidorna.
- `p` - skall ha fonttypen Georgia, följt av times och serif.
- `table` - tabellen skall ha kollapsande ramar, d.v.s ramarna mellan cellerna får inte ritas ut med dubbla streck utan ramarna för varje cell skall slås samman. Tabellen skall också ha en heldragen ram med tjockleken 3px i färgen #666666. Tabellen skall ha bredden 500 pixlar och vara centrerad.
- `td` - tabellcellerna skall ha en utfyllnad på 3 pixlar, en heldragen ram som är 1 pixel tjock och har färgen #666666. Slutligen skall tabellcellerna ha bredden 100 pixlar.
- `th` - tabellhuvudet skall ha klargul (#ff0), fet text mot svart bakgrund. Texten i dessa celler skall vara centrerad. I övrigt ska cellerna ha samma egenskaper som td.
- `tr:nth-child(even)` - tabell rader med jämna nummer skall ha bakgrundsfärgen #ffffaa.
- `tr:nth-child(odd)` - tabell rader med udda nummer skall ha bakgrundsfärgen #ffbbbb.
- `td.number` - tabellceller med siffor skall ha högerjusterad text.
- `footer` - skall ha en heldragen ram med bredden 1 pixel och färgen #999999. Den skall ha en marginal på 3 pixlar runt om. Texten skall vara 0.8em stor, vara *kursiverad* och centrerad i sidfoten. Länkfärgen i "footer" skall vara #1292FF.

Bortsett från detta ska ingen annan formatering göras.

## Bedömning

Denna uppgifts bedöms med antingen "Klarat", "Genomfört" eller "Inte genomfört"
