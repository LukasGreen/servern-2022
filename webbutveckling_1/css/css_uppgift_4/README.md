# CSS - Uppgift 4

I denna uppgift ska du få lära dig hur du gör en sida responsiv. Att den är responsiv betyder att den anpassar sig till den användarklient du använder. En responsiv sida är lika snygg och användbar på mobilen som på datorn. Läs gärna dessa delar i W3Schools innan du fortsätter:

- <https://www.w3schools.com/css/css_rwd_intro.asp>
- <https://www.w3schools.com/css/css_rwd_viewport.asp>
- <https://www.w3schools.com/css/css_rwd_mediaqueries.asp>
- <https://www.w3schools.com/css/css_rwd_images.asp>
- <https://www.w3schools.com/css/css_rwd_videos.asp>
- <https://www.w3schools.com/css/css_table_responsive.asp>

## Instruktioner

Utgå ifrån det HTML-dokument som följer med denna uppgift. Din uppgift är att få dokumentet att se bra ut, både på datorskärmen och i mobilen. Ett tips för att kontrollera hur sidan ser ut i en mobil är att använda dig av inspekteraren. I den kan du näst längst upp till vänster välja användarklient.

Följande krav gäller:

- **I alla lägen** ska body-elementet vara centrerat och maximalt 60em brett. Tabellen och alla bilderna ska göras responsiva. Deras bredd ska alltså anpassas till deras respektive elements bredd. Header-elementet ska ligga ovanför övriga element och footer-elementet ska ligga nederst på sidan.
- **I mobiltelefonens vertikala läge** ska alla delar av sidan hamna ovanför varandra. Inga delar av sidan ska alltså flyta. [Se facit-bilden](iphone_12_pro_vertical.png)
- **I ipadens vertikala läge** ska porträttbilden (i aside-elementet) ligga bredvid texten som hör ihop med den. I övrigt ska sidan ha en enda kolumn. [Se facit-bilden](ipad_mini_vertical.png)
- **I ipadens horisontella läge** ska sidan ha två kolumner med sidans inlägg på ena och informationen om författaren på den andra sidan. I detta läge ska porträttbilden, precis som i mobiltelefonens vertikala läge, ligga ovanför texten. [Se facit-bilden](ipad_mini_horizontal.png)